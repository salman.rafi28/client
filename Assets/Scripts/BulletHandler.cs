using UnityEngine;

public class BulletHandler : MonoBehaviour
{
    float Speed = 50f;
    private Vector3 Target;
    [SerializeField] private GameObject HitParticle;

    private void Start()
    {
        Target = GamePlayHandler.instance.HittingPoint;
        Destroy(gameObject, 2f);
    }

    void Update()
    {
        var step =  Speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, Target, step);
    }
    private void OnCollisionEnter(Collision other)
    {
        Destroy(gameObject);
        Instantiate(HitParticle, transform.position, transform.rotation);
        if (other.transform.GetComponent<TargetInt>() != null)
        {
            other.transform.GetComponent<TargetInt>().OnDestroy();
        }
    }
}
