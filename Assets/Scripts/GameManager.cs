using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private StarterAssetsDemo _MyInput;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            _MyInput = new StarterAssetsDemo();
        }
    }

    private void OnEnable()
    {
        _MyInput.Enable();
    }

    private void OnDisable()
    {
        _MyInput.Disable();
    }

    private void Update()
    {
        RestartLevel();
        GoToGamePlay();
        GoToFSMLevel();
    }

    public void RestartLevel()
    {
        if (_MyInput.Generic.Restart.triggered)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
    public void GoToGamePlay()
    {
        if (_MyInput.Generic.ShootingLevel.triggered)
        {
            ChangeScene("Task1-2");
        }
    }
    public void GoToFSMLevel()
    {
        if (_MyInput.Generic.FSMLevel.triggered)
        {
            ChangeScene("Task3");
        }
    }

    public void ChangeScene(string name)
    {
        SceneManager.LoadScene(name);
    }
    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 500, 20), "Press R To Reload Level");
        GUI.Label(new Rect(10, 20, 500, 20), "Press G For Task 1-2");
        GUI.Label(new Rect(10, 30, 500, 20), "Press F For Task 3");
        GUI.Label(new Rect(10, 40, 500, 20), "Press Space In FSM Level To Change State");
    }
}
