using UnityEngine;
using UnityEngine.AI;

public class StateManager : MonoBehaviour
{
    public Transform[] waypoints;
    
    BaseState currentState;
    
    [HideInInspector] public NavMeshAgent _agent;
    [HideInInspector] public Animator _animator;
    
    [HideInInspector] public IdleState _idleState ;
    [HideInInspector] public PatrolState _patrolState;
    
    public bool isPatrolling;
    [HideInInspector] public int currentWayPointIndex;
    [HideInInspector] public Transform currentTarget;
    
    private StarterAssetsDemo _MyInput;

    private void Awake()
    {
        _MyInput = new StarterAssetsDemo();
    }
    private void OnEnable()
    {
        _MyInput.Enable();
    }

    private void OnDisable()
    {
        _MyInput.Disable();
    }

    void Start()
    {
        _idleState = new IdleState();
        _patrolState = new PatrolState();
        currentState = _idleState;
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        
        currentState.EnterState(this);
    }

    private void Update()
    {
        if (_MyInput.AI.ChangeState.triggered)
        {
            currentState.UpdateState(this);
        }
        if (isPatrolling)
        {
            if (Vector3.Distance(transform.position, waypoints[currentWayPointIndex].position) < 2f)
            {
                IterateWaypoint();
            }
        }
    }
    void IterateWaypoint()
    {
        currentWayPointIndex++;

        if (currentWayPointIndex == waypoints.Length)
            currentWayPointIndex = 0;

        currentTarget = waypoints[currentWayPointIndex];
        
        _agent.SetDestination(currentTarget.position);
    }
    public void SwitchState(BaseState _nextState)
    {
        currentState = _nextState;
        _nextState.EnterState(this);
    }
}
