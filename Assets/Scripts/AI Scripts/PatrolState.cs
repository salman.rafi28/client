public class PatrolState : BaseState
{
    public override void EnterState(StateManager _stateManager)
    {
        _stateManager.currentTarget = _stateManager.waypoints[_stateManager.currentWayPointIndex];
        _stateManager._agent.Resume();
        _stateManager._animator.SetBool("isPatrol", true);
        _stateManager._agent.SetDestination(_stateManager.currentTarget.position);
        _stateManager.isPatrolling = true;
    }

    public override void UpdateState(StateManager _stateManager)
    {
        _stateManager.SwitchState(_stateManager._idleState);
        _stateManager._animator.SetBool("isPatrol", false);
        _stateManager.isPatrolling = false;
    }
}
