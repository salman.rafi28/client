public class IdleState : BaseState
{
    public override void EnterState(StateManager _stateManager)
    {
        _stateManager._agent.Stop();
        _stateManager._animator.Play("Idle");
    }

    public override void UpdateState(StateManager _stateManager)
    {
        _stateManager.SwitchState(_stateManager._patrolState);
    }

    


}
