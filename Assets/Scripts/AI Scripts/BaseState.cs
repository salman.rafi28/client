using UnityEngine;

public abstract class BaseState : MonoBehaviour
{
    public abstract void EnterState(StateManager _stateManager);    
    public abstract void UpdateState(StateManager _stateManager);    
}
