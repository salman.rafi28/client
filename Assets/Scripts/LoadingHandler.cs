using UnityEngine;

public class LoadingHandler : MonoBehaviour
{
    private void Start()
    {
        Invoke("ChangeToGamePlay", 2f);
    }

    void ChangeToGamePlay()
    {
        GameManager.instance.ChangeScene("Task1-2");
    }
}
