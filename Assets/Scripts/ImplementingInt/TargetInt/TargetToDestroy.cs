using UnityEngine;
using UnityEngine.UI;

public class TargetToDestroy : MonoBehaviour, TargetInt
{
    [SerializeField] private GameObject TextNumber;
    private int MyNumber;

    private void Awake()
    {
        TextNumber = Instantiate(TextNumber, Vector3.zero, Quaternion.identity);
        TextNumber.transform.SetParent(GamePlayHandler.instance.MainPanel.transform);
    }

    private void Update()
    {
        Vector3 NamePos = Camera.main.WorldToScreenPoint(transform.position);
        TextNumber.transform.position = NamePos;
    }

    public void NumberAssign(int index)
    {
        TextNumber.GetComponent<Text>().text = index.ToString();
        MyNumber = index;
    }

    public void OnDestroy()
    {
        if (GamePlayHandler.instance.CurrentCheckPoint == MyNumber)
        {
            GamePlayHandler.instance.CurrentCheckPoint++;
            Destroy(TextNumber);
            Destroy(gameObject);
        }
    }
}
