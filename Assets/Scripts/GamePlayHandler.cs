using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GamePlayHandler : MonoBehaviour
{
    public static GamePlayHandler instance;

    [Header("Panels")]
    public GameObject MainPanel;

    public Vector3 HittingPoint;

    [SerializeField] private GameObject[] SpheresToDestroy;
    private GameObject tempGameObject;
    [SerializeField] private Transform[] RandomPositions;
    
    private List<int> randNumForPositionsAssigned = new List<int>();
    private List<int> randNumAlreadyAssigned = new List<int>();
    private int randomNumber;
    
    private int NumberAssigner = 1;
    public int CurrentCheckPoint = 1;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        
    }
    
    private void Start()
    {
        for (int i = 0; i < SpheresToDestroy.Length; i++)
        {
            tempGameObject = Instantiate(SpheresToDestroy[GetUniqueNumber(SpheresToDestroy.Length, randNumAlreadyAssigned)],
                RandomPositions[GetUniqueNumber(RandomPositions.Length, randNumForPositionsAssigned)].position, Quaternion.identity);
            tempGameObject.GetComponent<TargetInt>().NumberAssign(NumberAssigner);
            NumberAssigner++;
        }
    }

    public int GetUniqueNumber(int RandomNumLength, List<int> RandomNumberListToUpdate)
    {
        randomNumber = Random.Range(0, RandomNumLength);

        while (RandomNumberListToUpdate.Contains(randomNumber))
        {
            randomNumber = Random.Range(0, RandomNumLength);
        }
        
        RandomNumberListToUpdate.Add(randomNumber);
        return randomNumber;
    }
}
